package com.example.kotlinfirebasemyfoodhist

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FileDownloadTask.TaskSnapshot
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import kotlinx.android.synthetic.main.activity_create_snap_two.*
import java.io.ByteArrayOutputStream
import java.util.*


class CreateSnapActivityTwo : AppCompatActivity() {


    private lateinit var addToDatabaseButton: Button
    var createSnapImageView: ImageView? = null
    private var mealName: EditText? = null
    private var carbTotalEditText: EditText? = null
    private var mealIngredientsEditText: EditText? = null
    private var raisesMmolByEditText: EditText? = null
    val imageName = UUID.randomUUID().toString() + ".jpg"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_snap_two)
        mealName = findViewById(R.id.nameOfFoodId)
        carbTotalEditText = findViewById(R.id.carbGmsTotal)
        mealIngredientsEditText = findViewById(R.id.mealIngredientsList)
        raisesMmolByEditText = findViewById(R.id.raisedSugar)
        addToDatabaseButton = findViewById(R.id.addToDatabaseButton)

        addToDatabaseButton.setOnClickListener {
            saveSugarDetailsToDatabase()
            addToDatabaseClicked()
            //            code to reload the Activity to reset all values to default hints
//            recreate();
            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }
        btn_scan.setOnClickListener {
            val scanner = IntentIntegrator(this)
            scanner.initiateScan()
        }

        

        btn_cam1.setOnClickListener { view ->
            var i=Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(i, 123)
        }

        createSnapImageView = findViewById(R.id.createSnapImageView)
//        messageEditText = findViewById(R.id.carbGmsTotal)
    }

//commented out the following lines (80-94) due to conflict with the following lines as per comment bellow...
    //    uncommented the following lines of code as the capture image button was not working on 31/05/2020 (lines 150-172)
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (resultCode == Activity.RESULT_OK) {
//            val result: IntentResult =
//                IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//            if (result != null) {
//                if (result.getContents() == null) {
//                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show();
//                }
//            } else {
//                super.onActivityResult(requestCode, resultCode, data);
//            }
//        }
//    }

    private fun saveSugarDetailsToDatabase() {

        val mealName1 = mealName?.text.toString().trim()

        if (mealName1.isEmpty()){
            mealName?.error = "Please enter meal name"
            return
        }

        val carbTotalEditText1 = carbTotalEditText?.text.toString().trim()

        if (carbTotalEditText1.isEmpty()){
            carbTotalEditText?.error = "Please enter carb total value"
            return
        }
        val mealIngredientsEditText1 = mealIngredientsEditText?.text.toString().trim()

        val raisesMmolByEditText1 = raisesMmolByEditText?.text.toString().trim()

        if (raisesMmolByEditText1.isEmpty()){
            raisesMmolByEditText?.error = "Please enter mmol value"
            return
        }

        val ref = FirebaseDatabase.getInstance().getReference("sugarRecords").child("Breakfast")
        val sugarRecordId = ref.push().key

        val sugarRecord = sugarRecordId?.let { SugarRecord(it, mealName1, carbTotalEditText1, raisesMmolByEditText1, mealIngredientsEditText1) }
        Log.i("This sugar record...", mealName1.toString())
        Log.i("This sugar record...", carbTotalEditText1.toString())
        Log.i("This sugar record...", mealIngredientsEditText1.toString())
        Log.i("This sugar record...", raisesMmolByEditText1.toString())
//        val sugarRecord = sugarRecordId?.let { SugarRecord(it, raisesMmolByEditText1) }
        if (sugarRecordId != null) {
            Log.i("This sugar record...", sugarRecord.toString())
            ref.child(sugarRecordId).setValue(sugarRecord).addOnCompleteListener{
                Toast.makeText(applicationContext, "Food record save successfully", Toast.LENGTH_LONG). show()
            }
        }

    }
//req004
fun  getPhoto() {
    val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    startActivityForResult(intent, 1)
}
    @RequiresApi(Build.VERSION_CODES.M)
    fun chooseImageClicked(view: View) {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1);
        } else
        {
            getPhoto()
        }
    }

//    uncommented the following lines of code as the capture image button was not working on 31/05/2020 (lines 150-172)
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        val selectedImage = data!!.data
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            try {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)

                createSnapImageView?.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if(requestCode==123) {
            var bmp= data?.extras?.get("data") as Bitmap
            createSnapImageView?.setImageBitmap(bmp)

        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoto()
            }
        }
    }

fun addToDatabaseClicked() {

    // Get the data from an ImageView as bytes
    // Get the data from an ImageView as bytes
    createSnapImageView?.setDrawingCacheEnabled(true)
    createSnapImageView?.buildDrawingCache()
    val bitmap = (createSnapImageView?.getDrawable() as BitmapDrawable).bitmap
    val baos = ByteArrayOutputStream()
    bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
    val data: ByteArray = baos.toByteArray()



    val uploadTask: UploadTask = FirebaseStorage.getInstance().getReference().child("images").child("BreakfastImages").child(imageName).putBytes(data)

//    val ref = storageRef.child("images").child("BreakfastImages").child(imageName)
//    uploadTask = ref.putFile(file)
//
//    val urlTask = uploadTask.continueWithTask { task ->
//        if (!task.isSuccessful) {
//            task.exception?.let {
//                throw it
//            }
//        }
//          UploadTask.downloadUrl
//    }.addOnCompleteListener { task ->
//        if (task.isSuccessful) {
//            val downloadUri = task.result
//        } else {
//            // Handle failures
//            // ...
//        }
//    }




    uploadTask.addOnFailureListener (OnFailureListener{
        // Handle unsuccessful uploads
        Toast.makeText(this, "UploadFailed", Toast.LENGTH_SHORT).show()
//    }).addOnCompleteListener { task ->
//        if (task.isSuccessful) {
//            val downloadUri = task.result
//            Log.i("URLdownloadXXX", downloadUri.toString())
//        } else {
//            // Handle failures
//            // ...
//        }
//
//    }
    }).addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot> { task ->
//         taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
       val downloadimagepath = task.getUploadSessionUri().toString();
//        val downloadimagepath1 = task.metadata.toString();
//        val downloadUrl = taskSnapshot.getMetadata()?.getReference()?.getDownloadUrl().toString()
//        val downloadUrl = taskSnapshot.getResult().toString()
//        val downloadUrl = taskSnapshot.metadata.toString()
//        val downloadUrl = TaskSnapshot.getResult().toString()
//        val downloadUrl = TaskSnapshot.getMetadata().toString()
//        val downloadUrl = uploadTask.getResult().metadata?.reference?.downloadUrl.toString()
//            val downloadUrl = taskSnapshot.metadata?.reference?.downloadUrl?.result.toString()
//        val downloadUrl = taskSnapshot.getStorage().getDownloadUrl().toString()
//        Log.i("URLdownloadXXX", downloadUrl.toString())
        Log.i("URLdownloadXXX", downloadimagepath)
//        Log.i("URLdownloadXXX1", downloadimagepath1)
        // ...
    })

}


}
