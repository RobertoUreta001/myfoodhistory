package com.example.kotlinfirebasemyfoodhist

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase

class SnapsActivity : AppCompatActivity() {

    val mAuth = FirebaseAuth.getInstance()
    var snapsListView: ListView? = null
    var snapsListViewLunch: ListView? = null
    var snapsListViewDinner: ListView? = null
    var snapsListViewSnack: ListView? = null
    var emails: ArrayList<String> = ArrayList()
    var breakfastFoods: ArrayList<String> = ArrayList()
    var lunchFoods: ArrayList<String> = ArrayList()
    var dinnerFoods: ArrayList<String> = ArrayList()
    var snackFoods: ArrayList<String> = ArrayList()
    var snaps: ArrayList<DataSnapshot> = ArrayList()

    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_snaps)

        val button = findViewById<ImageButton>(R.id.imageButtonBreakfast)
        button.setOnClickListener {
            val intent = Intent(this, CreateSnapActivityTwo::class.java)
            startActivity(intent)
        }
        val button1 = findViewById<ImageButton>(R.id.imageButtonLunch)
        button1.setOnClickListener {
            val intent = Intent(this, CreateSnapActivityLunch::class.java)
            startActivity(intent)
        }
        val button2 = findViewById<ImageButton>(R.id.imageButtonDinner)
        button2.setOnClickListener {
            val intent = Intent(this, CreateSnapActivityDinner::class.java)
            startActivity(intent)
        }
        val button3 = findViewById<ImageButton>(R.id.imageButtonSnack)
        button3.setOnClickListener {
            val intent = Intent(this, CreateSnapActivitySnack::class.java)
            startActivity(intent)
        }
        snapsListView = findViewById(R.id.snapsListView)
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, breakfastFoods)
        snapsListView?.adapter = adapter

        snapsListViewLunch = findViewById(R.id.lunchSnapsListView)
        val adapter1 = ArrayAdapter(this, android.R.layout.simple_list_item_1, lunchFoods)
        snapsListViewLunch?.adapter = adapter1

        snapsListViewDinner = findViewById(R.id.dinnerSnapsListView)
        val adapter2 = ArrayAdapter(this, android.R.layout.simple_list_item_1, dinnerFoods)
        snapsListViewDinner?.adapter = adapter2

        snapsListViewSnack = findViewById(R.id.snackSnapsListView)
        val adapter3 = ArrayAdapter(this, android.R.layout.simple_list_item_1, snackFoods)
        snapsListViewSnack?.adapter = adapter3

        mAuth.currentUser?.uid?.let {
            FirebaseDatabase.getInstance().getReference().child("sugarRecords").child("Breakfast").addChildEventListener(object: ChildEventListener {
                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    breakfastFoods.add(p0.child("mealName").value as String)
                    snaps.add(p0!!)
                    adapter.notifyDataSetChanged()
                }
                override fun onCancelled(p0: DatabaseError) {}
                override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
                override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

                override fun onChildRemoved(p0: DataSnapshot) {}

            })
//            try to display the database fields for sugar record when item from listview is clicked:
//            snapsListView?.onItemClickListener = adapterView.OnItemClickListener {adapterView, view, i, l ->
            snapsListView?.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
                val snapshot = snaps.get(i)

                var intent = Intent(this, ViewSugarRecordActivity::class.java)

                intent.putExtra("mealName",snapshot.child("mealName").value as String)
                intent.putExtra("carbTotal",snapshot.child("carbTotal").value as String)
                intent.putExtra("mealIngredients",snapshot.child("mealIngredients").value as String)
                intent.putExtra("raiseMmolBy",snapshot.child("raiseMmolBy").value as String)
//                intent.putExtra("snapKey",snapshot.key)

                startActivity(intent)
            }


            FirebaseDatabase.getInstance().getReference().child("sugarRecords").child("Lunch").addChildEventListener(object: ChildEventListener {
                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    lunchFoods.add(p0.child("mealName").value as String)
                    adapter1.notifyDataSetChanged()
                }
                override fun onCancelled(p0: DatabaseError) {}
                override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
                override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

                override fun onChildRemoved(p0: DataSnapshot) {}

            })

            FirebaseDatabase.getInstance().getReference().child("sugarRecords").child("Dinner").addChildEventListener(object: ChildEventListener {
                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    dinnerFoods.add(p0.child("mealName").value as String)
                    adapter2.notifyDataSetChanged()
                }
                override fun onCancelled(p0: DatabaseError) {}
                override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
                override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

                override fun onChildRemoved(p0: DataSnapshot) {}

            })
            FirebaseDatabase.getInstance().getReference().child("sugarRecords").child("Snack").addChildEventListener(object: ChildEventListener {
                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    snackFoods.add(p0.child("mealName").value as String)
                    adapter3.notifyDataSetChanged()
                }
                override fun onCancelled(p0: DatabaseError) {}
                override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
                override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

                override fun onChildRemoved(p0: DataSnapshot) {}

            })
        }
        fun goAndAddFoodItem() {
            val intent = Intent(this, CreateSnapActivityTwo::class.java)
            startActivity(intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val infalter = menuInflater
        infalter.inflate(R.menu.snaps,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.createSnap) {
            val intent = Intent(this, CreateSnapActivityTwo::class.java)
//            val intent = Intent(this, CreateSnapActivityTwo::class.java)
            startActivity(intent)
        } else if (item?.itemId == R.id.createSnapLunch) {
                val intent = Intent(this, CreateSnapActivityLunch::class.java)
//            val intent = Intent(this, CreateSnapActivityLunch::class.java)
                startActivity(intent)
        } else if (item?.itemId == R.id.createSnapDinner) {
            val intent = Intent(this, CreateSnapActivityDinner::class.java)
//            val intent = Intent(this, CreateSnapActivityTwo::class.java)
            startActivity(intent)
        } else if (item?.itemId == R.id.createSnapSnack) {
            val intent = Intent(this, CreateSnapActivitySnack::class.java)
//            val intent = Intent(this, CreateSnapActivityTwo::class.java)
            startActivity(intent)
        } else if (item?.itemId == R.id.createSnapDrink) {
            val intent = Intent(this, CreateSnapActivityDrink::class.java)
//            val intent = Intent(this, CreateSnapActivityTwo::class.java)
            startActivity(intent)
        } else  if (item?.itemId == R.id.logout) {
            mAuth.signOut()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        mAuth.signOut()
    }
}
