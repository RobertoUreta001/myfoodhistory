package com.example.kotlinfirebasemyfoodhist

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_create_snap_two.*
import java.io.ByteArrayOutputStream
import java.util.*

class CreateSnapActivityLunch : AppCompatActivity() {

    private lateinit var addToDatabaseButton: Button
    var createSnapImageView: ImageView? = null
    private var mealName: EditText? = null
    private var carbTotalEditText: EditText? = null
    private var mealIngredientsEditText: EditText? = null
    private var raisesMmolByEditText: EditText? = null
    val imageName = UUID.randomUUID().toString() + ".jpg"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_snap_lunch)
        mealName = findViewById(R.id.nameOfFoodId)
        carbTotalEditText = findViewById(R.id.carbGmsTotal)
        mealIngredientsEditText = findViewById(R.id.mealIngredientsList)
        raisesMmolByEditText = findViewById(R.id.raisedSugar)
        addToDatabaseButton = findViewById(R.id.addToDatabaseButton)



        addToDatabaseButton.setOnClickListener {
            saveSugarDetailsToDatabase()
            addToDatabaseClicked()
            //            code to reload the Activity to reset all values to default hints
//            recreate();
            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }



        btn_cam1.setOnClickListener { view ->
            var i=Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(i, 123)
        }

        createSnapImageView = findViewById(R.id.createSnapImageView)
//        messageEditText = findViewById(R.id.carbGmsTotal)
    }
    private fun saveSugarDetailsToDatabase() {

        val mealName1 = mealName?.text.toString().trim()

        if (mealName1.isEmpty()){
            mealName?.error = "Please enter meal name"
            return
        }

        val carbTotalEditText1 = carbTotalEditText?.text.toString().trim()

        if (carbTotalEditText1.isEmpty()){
            carbTotalEditText?.error = "Please enter carb total value"
            return
        }
        val mealIngredientsEditText1 = mealIngredientsEditText?.text.toString().trim()

        val raisesMmolByEditText1 = raisesMmolByEditText?.text.toString().trim()

        if (raisesMmolByEditText1.isEmpty()){
            raisesMmolByEditText?.error = "Please enter mmol value"
            return
        }

        val ref = FirebaseDatabase.getInstance().getReference("sugarRecords").child("Lunch")
        val sugarRecordId = ref.push().key

        val sugarRecord = sugarRecordId?.let { SugarRecord(it, mealName1, carbTotalEditText1, raisesMmolByEditText1, mealIngredientsEditText1) }
        Log.i("This sugar record...", mealName1.toString())
        Log.i("This sugar record...", carbTotalEditText1.toString())
        Log.i("This sugar record...", mealIngredientsEditText1.toString())
        Log.i("This sugar record...", raisesMmolByEditText1.toString())
//        val sugarRecord = sugarRecordId?.let { SugarRecord(it, raisesMmolByEditText1) }
        if (sugarRecordId != null) {
            Log.i("This sugar record...", sugarRecord.toString())
            ref.child(sugarRecordId).setValue(sugarRecord).addOnCompleteListener{
                Toast.makeText(applicationContext, "Food record save successfully", Toast.LENGTH_LONG). show()
            }
        }

    }
    //req004
    fun  getPhoto() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 1)
    }
    @RequiresApi(Build.VERSION_CODES.M)
    fun chooseImageClicked(view: View) {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1);
        } else
        {
            getPhoto()
        }
    }
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        val selectedImage = data!!.data
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            try {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)

                createSnapImageView?.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if(requestCode==123) {
            var bmp= data?.extras?.get("data") as Bitmap
            createSnapImageView?.setImageBitmap(bmp)

        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoto()
            }
        }
    }

    fun addToDatabaseClicked() {

        // Get the data from an ImageView as bytes
        // Get the data from an ImageView as bytes
        createSnapImageView?.setDrawingCacheEnabled(true)
        createSnapImageView?.buildDrawingCache()
        val bitmap = (createSnapImageView?.getDrawable() as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data: ByteArray = baos.toByteArray()



        val uploadTask: UploadTask = FirebaseStorage.getInstance().getReference().child("images").child("LunchImages").child(imageName).putBytes(data)
        uploadTask.addOnFailureListener (OnFailureListener{
            // Handle unsuccessful uploads
            Toast.makeText(this, "UploadFailed", Toast.LENGTH_SHORT).show()
        }).addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot> { taskSnapshot ->
            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
            val downloadUrl = taskSnapshot.getMetadata()?.getReference()?.getDownloadUrl().toString()
            Log.i("URL", downloadUrl.toString())
            // ...
        })

    }
}
