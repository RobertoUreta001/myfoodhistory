package com.example.kotlinfirebasemyfoodhist

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import java.net.HttpURLConnection
import java.net.URL

class ViewSugarRecordActivity : AppCompatActivity() {

    var messageTextView: TextView? = null
    var carbTotalView: TextView? = null
    var mealIngredientsView: TextView? = null
    var raiseMmolByView: TextView? = null
    var snapImageView: ImageView? = null
    val mAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_sugar_record)

        messageTextView = findViewById(R.id.nameOfFoodId)
        carbTotalView = findViewById(R.id.carbGmsTotal)
        mealIngredientsView = findViewById(R.id.mealIngredientsList)
        raiseMmolByView = findViewById(R.id.raisedSugar)
        snapImageView = findViewById(R.id.snapImageView)


        messageTextView?.text = intent.getStringExtra("mealName")
        carbTotalView?.text = intent.getStringExtra("carbTotal")
        mealIngredientsView?.text = intent.getStringExtra("mealIngredients")
        raiseMmolByView?.text = intent.getStringExtra("raiseMmolBy")


        val task = ImageDownloader()
        val myImage: Bitmap
        try {
            myImage = task.execute(intent.getStringExtra("imageURL")).get()

            snapImageView?.setImageBitmap(myImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    inner class ImageDownloader : AsyncTask<String, Void, Bitmap>() {

        override fun doInBackground(vararg urls: String): Bitmap? {

            try {
                val url = URL(urls[0])

                val connection = url.openConnection() as HttpURLConnection

                connection.connect()

                val `in` = connection.inputStream

                return BitmapFactory.decodeStream(`in`)

            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }

        }
    }
}
